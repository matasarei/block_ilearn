<?php
/**
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../config.php');

$id = optional_param('id', 0, PARAM_INT);

$DB->delete_records('ilearn_menu_block', ['id' => $id]);

redirect(new moodle_url('/blocks/ilearn/edit_items.php'));