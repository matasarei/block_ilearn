$(document).ready(function () {
    if ($().sortable) {
        var table = $('.bi-items-list');
        table.sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function (e) {
                var order = [];
                table.find('[data-id]').each(function () {
                    order.push($(this).data('id'));
                });

                $.ajax({
                    type: "POST",
                    url: "/blocks/ilearn/actions/order.php",
                    data: {
                        order: order
                    },
                    success: function () {

                    }
                });
            }
        });

        $('[data-action=order]').css('cursor', 'move').click(function () {
            return false;
        }).removeAttr('href');
    } else {
        console.log('sortable.js required');
    }
    
    $('[data-action=remove]').click(function(e) {
        return confirm("You going to remove this item. Continue?");
    });
});