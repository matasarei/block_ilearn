<?php
/**
 * https://docs.moodle.org/dev/AJAX
 *
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 */

define('AJAX_SCRIPT', true);
require_once(__DIR__ . '/../../../config.php');

$data_order = filter_input(INPUT_POST, 'order', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
foreach ($data_order as $order => $id) {
    $sql = "UPDATE {ilearn_menu_block} 
               SET ord = ?
             WHERE id = ?";
    $DB->execute($sql, [$order, $id]);
}
