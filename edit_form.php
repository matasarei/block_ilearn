<?php
/**
 * Edit form
 * http://docs.moodle.org/dev/
 *
 * @package    block_ilearn
 * @copyright  Yevhen Matasar <matasar.ei@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.');

class block_ilearn_edit_form extends block_edit_form {
    
    /**
     * Defines edit form
     * @param moodleform $mform
     */
    protected function specific_definition($mform) {
        $mform->addElement('select', 'config_teacher_links', get_string('teacher_links', 'block_ilearn'), [
            block_ilearn::TEACHER_LINKS_ENABLED => get_string('yes'),
            block_ilearn::TEACHER_LINKS_DISABLED => get_string('no')
        ]);
    }
}