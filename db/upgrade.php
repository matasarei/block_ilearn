<?php
/**
 * http://docs.moodle.org/dev/Upgrade_API
 * http://docs.moodle.org/dev/Data_definition_API
 * http://docs.moodle.org/dev/XMLDB_creating_new_DDL_functions
 * http://docs.moodle.org/dev/DB_layer_2.0_migration_docs
 *
 * WARNING! Since Moodle 2.0 enum support is TOTALY dropped! Use char instead.
 * Зверніть увагу, що з версії 2.0 розробники вирішили ПОВНІСТЮ відмовитися від enum типу.
 * Це означає що усі подібні поля необхідно замінити на звичайні текстові.
 *
 * @package    local_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// It must be included from a Moodle page.
defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.'); 

/**
 * Performs upgrade of the database structure and data
 *
 * @param int $oldversion the version we are upgrading from
 * @return bool true
 */
function xmldb_block_ilearn_upgrade($oldversion = 0) {
    global $DB;

    $dbman = $DB->get_manager();
    
    if ($oldversion < 2017090900) {
        $table = new xmldb_table('ilearn_menu_block');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        
        // url
        $table->add_field('url', XMLDB_TYPE_CHAR, '256', null, XMLDB_NOTNULL);
        
        // name
        $table->add_field('name', XMLDB_TYPE_CHAR, '64', null, XMLDB_NOTNULL);
        
        // title
        $table->add_field('title', XMLDB_TYPE_CHAR, '64', null, XMLDB_NOTNULL);
        
        // order
        $table->add_field('order', XMLDB_TYPE_INTEGER, '3', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, 0);
        
        // hidden
        $table->add_field('hidden', XMLDB_TYPE_INTEGER, '1', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, 0);
        
        // setup primary key.
        $primary = new xmldb_key('primary');
        $primary->set_attributes(XMLDB_KEY_PRIMARY, ['id'], null, null);
        $table->addKey($primary);

        $dbman->create_table($table);
    }
    
    return true;
}