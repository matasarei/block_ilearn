<?php
/**
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../config.php');
require_once("{$CFG->dirroot}/blocks/ilearn/item_edit_form.php");

// Check access.
require_login();

$id = optional_param('id', 0, PARAM_INT);

// Setting up page.
$PAGE->set_context(context_system::instance());
$PAGE->set_url("/blocks/ilearn/edit_item.php");
$PAGE->set_title(get_string('edit_items', 'block_ilearn'));
$PAGE->set_heading(get_string('edit_items', 'block_ilearn'));
$PAGE->set_pagelayout("coursecategory");

$form = new item_edit_form($id);
if ($form->is_cancelled()) {
    redirect(new moodle_url('/blocks/ilearn/edit_items.php'));
}

$data = $form->get_data();
if ($data) {
    $item = new stdClass();
    $item->url = $data->url;
    $item->name = $data->name;
    $item->title = $data->title;
    $item->hidden = (int)$data->hidden;
    if ($data->id) {
        $item->id = $data->id;
        $DB->update_record('ilearn_menu_block', $item);
    } else {
        $item->ord = 0;
        $DB->insert_record('ilearn_menu_block', $item);
    }
    redirect(new moodle_url('/blocks/ilearn/edit_items.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string($id ? 'edit_item' : 'add_item', 'block_ilearn'));
$form->display();
echo $OUTPUT->footer();