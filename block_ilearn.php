<?php
/**
 * iLearn menu
 * http://docs.moodle.org/dev/
 *
 * @package    block_ilearn
 * @copyright  Yevhen Matasar <matasar.ei@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die('Direct access to this script is forbidden.');

class block_ilearn extends block_base {
    
    public function __construct() {
        global $CFG;
        require_once "{$CFG->dirroot}/local/ilearn/lib.php";
        parent::__construct();
    }
    
    /**
     * Init function
     * @return void
     */
    function init() {
        $this->title = get_string('pluginname','block_ilearn');
    }
    
    const TEACHER_LINKS_ENABLED = 'tle';
    const TEACHER_LINKS_DISABLED = 'tld';
    
    /**
     * Returns content of the block
     * @return string Content of the block
     */
    function get_content() {
        $context = context_system::instance();
        $this->content = new stdClass();
        $html = "";
        
        $actions = [];
        if (has_capability('block/ilearn:edititems', $context)) {
            $url = new moodle_url('/blocks/ilearn/edit_items.php');
            $actions[] = html_writer::link($url, '&#9998;' . get_string('edit_items', 'block_ilearn'));
        }
        
        $html .= html_writer::tag('div', html_writer::alist($actions), ['class' => 'bi-actions']);

        $items = [];
        $this->_teacherLinks($items);
        $this->_getDBItems($items);
        
        $this->_genMenu($items, $html);
        $this->content->text = html_writer::tag('div', $html, ['class' => 'bi-menu']);
        return $this->content;
    }
    
    private function _getDBItems(&$items) {
        global $DB;
        
        $sql = "SELECT * FROM {ilearn_menu_block} WHERE hidden != '1' ORDER BY ord";
        $items = array_merge($items, $DB->get_records_sql($sql));
    }
    
    /**
     * 
     * @param type $items
     * @param type $html
     * @return type
     */
    private function _genMenu($items, &$html = "") {
        $parts = [];
        foreach ($items as $item) {
            $part = "";
            if ($item->title) {
                $part .= html_writer::tag('div', $item->title, ['bi-item-title']);
            }
            $part .= html_writer::link($item->url, $item->name, ['class' => 'bi-item-link']);
            $parts[] = html_writer::tag('li', html_writer::tag('div', $part, [
                'class' => "bi-item-container"
            ]), ['class' => "bi-item bi-item-{$item->class}"]);
        }

        $html = html_writer::tag('div', implode("\n", $parts), ['class' => 'bi-items']);
        return $html;
    }
    
    /**
     * 
     * @param moodle_url $url
     * @param type $name
     * @param type $items
     * @param type $title
     */
    private function _genItem(moodle_url $url, $name, array &$items, $title = null, $class = null) {
        $item = new stdClass();
        $item->id = 0;
        $item->url = $url;
        $item->name = empty($name) ? $url->get_path() : $name;
        $item->class = $class;
        $item->title = $title;
        $item->order = $items ? min(array_keys($items)) - 1 : 0;
        array_unshift($items, $item);
    }
    
    /**
     * 
     * @param type $items
     */
    private function _teacherLinks(&$items) {
        global $USER;
        
        if (!isloggedin()) {
            return false;
        }
        
        $allow_teacher_links = ilearn_is_teacher() || !ilearn_is_student();
        if ($this->config->teacher_links === self::TEACHER_LINKS_ENABLED && $allow_teacher_links) {
            $url = new moodle_url('/local/ilearn/teacher/');
            $name = get_string(ilearn_is_teacher() ? 'teachers_page' : 'become_teacher', 'local_ilearn');
            $this->_genItem($url, $name, $items, null, 'teacher');
        }
        
        $courseid = ilearn_is_course_page();
        if ($courseid) {
            $user = ilearn_get_teacher($courseid);
            $coursecontext = context_course::instance($courseid);
            if ($user && $user->id != $USER->id && has_capability('local/ilearn:manageteachers', $coursecontext)) {
                $name = get_string('teachers_page', 'local_ilearn') . ' (' . fullname($user) . ')';
                $url = new moodle_url("/local/ilearn/teacher/", ['user' => $user->id]);
                $this->_genItem($url, $name, $items, null, 'teacher-other');
            }
        }
        return true;
    }
}