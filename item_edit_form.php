<?php
/**
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 */

require_once("{$CFG->libdir}/formslib.php");

class item_edit_form extends \moodleform {
    
    private $_id = 0;
    private $_url = "#";
    private $_name = "";
    private $_title = "";
    private $_hidden = false;
    
    public function __construct($id = 0) {
        global $DB;
        
        if ($id) {
            $item = $DB->get_record('ilearn_menu_block', ['id' => $id], '*', MUST_EXIST);
            $this->_id = $item->id;
            $this->_name = $item->name;
            $this->_title = $item->title;
            $this->_url = $item->url;
            $this->_hidden = (bool)$item->hidden;
        }
        
        parent::__construct();
    }
    
    public function definition() {
        $mform = $this->_form;
        
        // id.
        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $this->_id);
        
        // url.
        $mform->addElement('text', 'url', get_string('item_url', 'block_ilearn'), ['size' => 64]);
        $mform->setType('url', PARAM_TEXT);
        $mform->setDefault('url', $this->_url);
        $mform->addRule('url', get_string('error'), 'required');
        
        // item name.
        $mform->addElement('text', 'name', get_string('item_name', 'block_ilearn'), ['size' => 64]);
        $mform->setType('name', PARAM_TEXT);
        $mform->setDefault('name', $this->_name);

        // item title.
        $mform->addElement('text', 'title', get_string('item_title', 'block_ilearn'), ['size' => 64]);
        $mform->setType('title', PARAM_TEXT);
        $mform->setDefault('title', $this->_title);
        
        // hidden.
        $mform->addElement('advcheckbox', 'hidden', get_string('hidden', 'block_ilearn'), null, ['group' => 1], [0, 1]);
        $mform->setDefault('hidden', $this->_hidden);
        
        // actions.
        $mform->addGroup([
            $mform->createElement('submit', 'submitbutton', get_string('save_item', 'block_ilearn')),
            $mform->createElement('cancel')
        ], 'buttonarr', '', [' '], false);
    }
    
}