<?php
/**
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

// Config and system init.
require_once(__DIR__ . '/../../config.php');

// Check access.
require_login();

// Setting up page.
$PAGE->set_context(context_system::instance());
$PAGE->set_url("/blocks/ilearn/edit_items.php");
$PAGE->set_title(get_string('edit_items', 'block_ilearn'));
$PAGE->set_heading(get_string('edit_items', 'block_ilearn'));
$PAGE->set_pagelayout("coursecategory");

$PAGE->requires->jquery();
$PAGE->requires->js('/blocks/ilearn/js/sortable.js');
$PAGE->requires->js('/blocks/ilearn/js/start.js');

echo $OUTPUT->header();

$page_actions = [
    html_writer::link(new moodle_url('/blocks/ilearn/edit_item.php'), '&#10010; ' . get_string('add_item', 'block_ilearn')),
    html_writer::link($PAGE->url, '&#x267B; ' . get_string('refresh'))
];
echo html_writer::alist($page_actions, ['class' => 'nav nav-pills']);

$table = new html_table();
$table->attributes = ['class' => 'generaltable bi-items-list'];
$table->head = [
    "",
    "ID",
    get_string('item_url', 'block_ilearn'),
    get_string('item_name', 'block_ilearn'),
    get_string('item_title', 'block_ilearn'),
    get_string('hidden', 'block_ilearn'),
    ""
];

$items = $DB->get_records_sql('SELECT * FROM {ilearn_menu_block} ORDER BY ord');
foreach ($items as $item) {
    $actions = [
        html_writer::link(new moodle_url('/blocks/ilearn/edit_item.php', [
            'id' => $item->id
        ]), '[' .get_string('edit_item', 'block_ilearn') . ']'),
        html_writer::link(new moodle_url('/blocks/ilearn/remove_item.php', [
            'id' => $item->id
        ]), '[' .get_string('remove_item', 'block_ilearn') . ']', [
            'style' => 'color:red', 'data-action' => 'remove'
        ])
    ];
    
    $table->data[] = [
        html_writer::link(new moodle_url('/blocks/ilearn/edit_item.php', [
            'id' => $item->id, 'change_order' => true
        ]), html_writer::empty_tag('img', ['src' => $OUTPUT->pix_url('t/sort', 'core')]), [
            'data-action' => 'order',
            'data-id' => $item->id
        ]),
        $item->id,
        html_writer::link($item->url, $item->url, ['target' => '_blank']),
        $item->name,
        $item->title,
        get_string($item->hidden ? 'yes' : 'no'),
        implode(" | ", $actions)
    ];
}

echo html_writer::table($table);
echo $OUTPUT->footer();