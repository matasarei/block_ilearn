<?php
/**
 * Languange strings
 *
 * @package    block_ilearn
 * @author     Yevhen Matasar <matasar.ei@gmail.com>
 * @link       https://docs.moodle.org/dev/
 */

///
// General.
///
$string['pluginname'] = "iLearn menu";
$string['title'] = "iLearn menu";

$string['edit_items'] = "Edit items";
$string['teacher_links'] = "Teacher links";

$string['add_item'] = "Add item";
$string['edit_item'] = "Edit item";
$string['remove_item'] = "Remove item";
$string['item_url'] = "Item url";
$string['item_name'] = "Item name";
$string['item_title'] = "Item title";
$string['save_item'] = "Save item";
$string['hidden'] = "Hidden";